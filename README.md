# Dense Road Surface Grip Map Prediction from Multimodal Image Data

This repository contains the inference code for the paper *Dense Road Surface Grip Map Prediction from Multimodal Image Data* alongside a few datasamples. For the installation and a quick demonstration refer to the section **Demo**.

![Main idea](/main_idea.png "Grip map prediction framework")

## Demo

The package requirements are made for Python 3.10. For older Python versions choose packge versions manually or omit the version numbers in *requirements.txt*. However, conflicts may appear. 

To change if the model is run using CPU or GPU, change the *device* variable to 'cpu' or 'cuda' in the *demo.py* file correspondingly. The model can be also changed between the RGB and RGB+T+R models by changing the boolean *RGB* variable.

```
git clone https://gitlab.com/fgi_nls/public/grip-prediction.git
cd grip-prediction
pip install -r requirements.txt
python3 demo.py
```

Alternatively use the Jupyter notebook *demo.ipynb*. To install the ipykernel required by the notebook, uncomment the last row in *requirements.txt* before ```pip install -r requirements.txt```.

If you do not have git-lfs installed download trained model weights manually and place them in the project directory: 
- [RGB](https://gitlab.com/fgi_nls/public/grip-prediction/-/raw/main/rgb.pt?inline=false)
- [RGB+T+R](https://gitlab.com/fgi_nls/public/grip-prediction/-/raw/main/rgb_t_r.pt?inline=false)
