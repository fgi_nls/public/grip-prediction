import os

RW_DATAS = ['Water', 'Ice', 'Snow']

N_INPUT_CHANNELS = 3

FGIMULTIMODAL_FOLDER = ''

ORIG_IMAGE_SIZE = (540, 1836)
HORIZON_LEVEL_PIX = 50 - 22 # pixels from top in FGI data, -22 from 32 pix cropping

RGB_MEAN = [0.485, 0.456, 0.406] 
RGB_STD = [0.229, 0.224, 0.225] 

REFLECTANCE_MEAN = 8.39345
REFLECTANCE_STD = 14.343518698670064
REFLECTANCE_ZERO = -REFLECTANCE_MEAN/REFLECTANCE_STD

ROAD_REFLECTANCE_MEAN = 5.333301
ROAD_REFLECTANCE_STD = 9.046043963516079
ROAD_REFLECTANCE_ZERO = -ROAD_REFLECTANCE_MEAN/ROAD_REFLECTANCE_STD

# These are calculated with weights based on samples and with max value clipping (water 20, ice 3.5, snow 2)
RW_NORM_MEAN = {'Grip': 0.524126, 'Water': 0.5542798, 'Ice': 0.05985061, 'Snow': 0.188797} # add mean and std values for vaisala measurements # TODO calculate more if needed
RW_NORM_STD = {'Grip': 0.19028655, 'Water': 1.8612958, 'Ice': 0.12354839, 'Snow': 0.36547163} # based on frames