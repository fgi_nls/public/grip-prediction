# %% [markdown]
# ### Notebook for demonstrating dense grip map prediction inference

# %%
RGB = False # Change to False for fusion model
device = "cpu" # Change to 'cpu' to run without gpu

# %%
import torch
import config
from models import custom_smp_model, middle_fusion
from utils.fix_weights import load_fixed_weights
from torch.utils.data import DataLoader
from utils.demoset import DemoSet
from utils.plot_outputs import plot_image
import matplotlib.pyplot as plt

# %% [markdown]
# #### Load RGB or fusion model

# %%
if RGB:
    model = custom_smp_model.SMPModel(model="FPN",
                                        encoder="resnet18",
                                        in_channels=3,
                                        out_channels=4)
    model = load_fixed_weights(model, "rgb.pt", device=device)
else:
    model = middle_fusion.MiddleFusionFPN(modalities=['rgb','thermal','reflectance'],
                                                    encoder_rgb='resnet18',
                                                    encoder_thermal='resnet18',
                                                    encoder_reflectance='resnet18',
                                                    out_channels=4,
                                                    dropout=0.2,
                                                    fusion_merge='cat')
    model = load_fixed_weights(model, "rgb_t_r.pt", device=device)
model.to(device)

# %% [markdown]
# #### Initialise demoloader

# %%
inputs = ['rgb']
if not RGB:
    inputs.append('thermal')
    inputs.append('reflectance')
testset = DemoSet('Test', 'data', inputs=inputs)
demoloader = DataLoader(testset, batch_size=1, num_workers=8, drop_last=False)

# %% [markdown]
# #### Iterate demoloader and plot RGB inputs and outputs

# %%
fig = plt.figure()
for i, batch in enumerate(demoloader):
    inputs = [batch['rgb'].to(device)]
    if not RGB:
        inputs.append(batch['thermal'].to(device))
        inputs.append(batch['reflectance'].to(device))
    input = torch.cat(inputs, dim=1)
    # Generate outputs
    outputs = model(input)
    
    # The output colourmap is normalised to the range [0.1, 0.82]
    plot_image(batch, outputs, notebook=False, fig=fig, idx=i)

fig.tight_layout()
plt.show()