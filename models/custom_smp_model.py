import segmentation_models_pytorch as smp
import torch.nn as nn
import torch

class SMPModel(nn.Module):
    def __init__(self, model="Unet", encoder="resnet18", pretraining=None, in_channels=3, out_channels=1, dropout=0.2):
        super(SMPModel, self).__init__()

        model_class = getattr(smp, model)
        if model == "FPN":
            self.model = model_class(encoder,
                                 encoder_weights=pretraining,
                                 in_channels=in_channels,
                                 classes=out_channels,
                                 decoder_dropout=dropout)
        else:
            self.model = model_class(encoder,
                                    encoder_weights=pretraining,
                                    in_channels=in_channels,
                                    classes=out_channels)

    def forward(self, x):
        return self.model(x)

