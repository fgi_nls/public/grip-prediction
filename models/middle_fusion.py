import segmentation_models_pytorch as smp
import torch.nn as nn
import torch
import numpy as np

from segmentation_models_pytorch.base import initialization as init

from models.cbam import CBAM

class MiddleFusionFPN(nn.Module):
    def __init__(self, modalities, encoder_rgb='resnet18', encoder_thermal='resnet18', encoder_reflectance='resnet18', out_channels=1, dropout=0.2, fusion_merge='cat'):
        super(MiddleFusionFPN, self).__init__()

        #encoder_depth = 5
        decoder_pyramid_channels = 256
        decoder_segmentation_channels = 128
        decoder_merge_policy = "add"
        activation = None
        upsampling = 4
        self.fusion_merge = fusion_merge

        self.modalities = modalities
        self.encoders = {}
        self.rgb_encoder = None
        self.thermal_encoder = None
        self.reflectance_encoder = None
        if 'rgb' in self.modalities:
            self.rgb_encoder = smp.encoders.get_encoder(encoder_rgb, in_channels=3, depth=5, weights=None)
            self.encoders['rgb'] = self.rgb_encoder
        if 'thermal' in self.modalities:
            self.thermal_encoder = smp.encoders.get_encoder(encoder_thermal, in_channels=1, depth=5, weights=None)
            self.encoders['thermal'] = self.thermal_encoder
        if 'reflectance' in self.modalities:
            self.reflectance_encoder = smp.encoders.get_encoder(encoder_reflectance, in_channels=1, depth=5, weights=None)
            self.encoders['reflectance'] = self.reflectance_encoder

        #self.encoder_channels = [[sum(enc.out_channels[i]) for enc in range(len(self.encoders[self.modalities]))] for i in range(len(self.encoders[self.modalities[0].out_channels]))]
        if self.fusion_merge == 'cat':
            enc_out_channels = [enc.out_channels for enc in self.encoders.values()]
            self.encoder_out_channels = tuple(np.sum(np.array(enc_out_channels), axis=0))
        else:
            self.encoder_out_channels = self.encoders[self.modalities[0]].out_channels

        self.decoder = smp.decoders.fpn.decoder.FPNDecoder(encoder_channels=self.encoder_out_channels,
                                                           encoder_depth=5,
                                                           pyramid_channels=decoder_pyramid_channels,
                                                           segmentation_channels=decoder_segmentation_channels,
                                                           merge_policy=decoder_merge_policy,
                                                           dropout=dropout)

        self.head = smp.base.SegmentationHead(in_channels=self.decoder.out_channels,
                                              out_channels=out_channels,
                                              activation=activation,
                                              kernel_size=1,
                                              upsampling=upsampling)

        init.initialize_decoder(self.decoder)
        init.initialize_head(self.head)

    def forward(self, x):
        last_i = 0
        encoder_outs = []
        if 'rgb' in self.modalities:
            encoder_outs.append(self.rgb_encoder(x[:,0:3,:,:]))
            last_i = 3
        if 'thermal' in self.modalities:
            encoder_outs.append(self.thermal_encoder(x[:,last_i:(last_i+1),:,:]))
            last_i += 1
        if 'reflectance' in self.modalities:
            encoder_outs.append(self.reflectance_encoder(x[:,last_i:(last_i+1),:,:]))
        features = []
        for i in range(len(encoder_outs[0])):
            if self.fusion_merge == 'cat':
                #print([encoder_outs[i][j].shape for j in range(len(self.modalities))])
                features.append(torch.cat([encoder_outs[j][i] for j in range(len(self.modalities))], dim=1))
            elif self.fusion_merge == 'add':
                features.append(sum([encoder_outs[j][i] for j in range(len(self.modalities))]))
        '''
        x = [self.encoders[mod](x[mod]) for mod in self.modalities]
        features = []
        for i in range(len(x[0])):
            if self.fusion_merge == 'cat':
                features.append(torch.cat([x[i][j] for j in range(len(self.modalities))], dim=1))
            elif self.fusion_merge == 'sum':
                features.append(sum([x[i][j] for j in range(len(self.modalities))]))
        '''
        decoder_out = self.decoder(*features)

        return self.head(decoder_out) 

class MiddleFusionBAMFPN(nn.Module):
    def __init__(self, modalities, encoder_rgb='resnet18', encoder_thermal='resnet18', encoder_reflectance='resnet18', out_channels=1, dropout=0.2, fusion_merge='cat'):
        super(MiddleFusionBAMFPN, self).__init__()

        #encoder_depth = 5
        decoder_pyramid_channels = 256
        decoder_segmentation_channels = 128
        decoder_merge_policy = "add"
        activation = None
        upsampling = 4
        self.fusion_merge = fusion_merge

        self.modalities = modalities
        self.encoders = {}
        self.rgb_encoder = None
        self.thermal_encoder = None
        self.reflectance_encoder = None
        if 'rgb' in self.modalities:
            self.rgb_encoder = smp.encoders.get_encoder(encoder_rgb, in_channels=3, depth=5, weights=None)
            self.encoders['rgb'] = self.rgb_encoder
        if 'thermal' in self.modalities:
            self.thermal_encoder = smp.encoders.get_encoder(encoder_thermal, in_channels=1, depth=5, weights=None)
            self.encoders['thermal'] = self.thermal_encoder
        if 'reflectance' in self.modalities:
            self.reflectance_encoder = smp.encoders.get_encoder(encoder_reflectance, in_channels=1, depth=5, weights=None)
            self.encoders['reflectance'] = self.reflectance_encoder

        #self.encoder_channels = [[sum(enc.out_channels[i]) for enc in range(len(self.encoders[self.modalities]))] for i in range(len(self.encoders[self.modalities[0].out_channels]))]
        if self.fusion_merge == 'cat':
            enc_out_channels = [enc.out_channels for enc in self.encoders.values()]
            self.encoder_out_channels = tuple(np.sum(np.array(enc_out_channels), axis=0))
        else:
            self.encoder_out_channels = self.encoders[self.modalities[0]].out_channels
        self.bam1 = CBAM(self.encoder_out_channels[-1], no_spatial=True)
        self.bam2 = CBAM(self.encoder_out_channels[-2], no_spatial=True)
        self.bam3 = CBAM(self.encoder_out_channels[-3], no_spatial=True)
        self.bam4 = CBAM(self.encoder_out_channels[-4], no_spatial=True)
        self.decoder = smp.decoders.fpn.decoder.FPNDecoder(encoder_channels=self.encoder_out_channels,
                                                           encoder_depth=5,
                                                           pyramid_channels=decoder_pyramid_channels,
                                                           segmentation_channels=decoder_segmentation_channels,
                                                           merge_policy=decoder_merge_policy,
                                                           dropout=dropout)

        self.head = smp.base.SegmentationHead(in_channels=self.decoder.out_channels,
                                              out_channels=out_channels,
                                              activation=activation,
                                              kernel_size=1,
                                              upsampling=upsampling)

        init.initialize_decoder(self.decoder)
        init.initialize_head(self.head)

    def forward(self, x):
        last_i = 0
        encoder_outs = []
        if 'rgb' in self.modalities:
            encoder_outs.append(self.rgb_encoder(x[:,0:3,:,:]))
            last_i = 3
        if 'thermal' in self.modalities:
            encoder_outs.append(self.thermal_encoder(x[:,last_i:(last_i+1),:,:]))
            last_i += 1
        if 'reflectance' in self.modalities:
            encoder_outs.append(self.reflectance_encoder(x[:,last_i:(last_i+1),:,:]))
        features = []
        for i in range(len(encoder_outs[0])):
            if self.fusion_merge == 'cat':
                #print([encoder_outs[i][j].shape for j in range(len(self.modalities))])
                features.append(torch.cat([encoder_outs[j][i] for j in range(len(self.modalities))], dim=1))
            elif self.fusion_merge == 'add':
                features.append(sum([encoder_outs[j][i] for j in range(len(self.modalities))]))
        '''
        x = [self.encoders[mod](x[mod]) for mod in self.modalities]
        features = []
        for i in range(len(x[0])):
            if self.fusion_merge == 'cat':
                features.append(torch.cat([x[i][j] for j in range(len(self.modalities))], dim=1))
            elif self.fusion_merge == 'sum':
                features.append(sum([x[i][j] for j in range(len(self.modalities))]))
        '''
        features[-1] = self.bam1(features[-1])
        features[-2] = self.bam2(features[-2])
        features[-3] = self.bam3(features[-3])
        features[-4] = self.bam4(features[-4])
        decoder_out = self.decoder(*features)

        return self.head(decoder_out) 

