matplotlib==3.8.0
numpy==1.23.5
opencv_contrib_python==4.8.1.78
opencv_python==4.5.5.64
pandas==2.2.1
Pillow==10.0.1
scipy==1.13.0
segmentation_models_pytorch==0.3.3
torch==2.1.0
torchvision==0.16.0
# For running the notebook demo 
# ipykernel==6.29.4 