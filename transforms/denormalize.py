import numpy as np
import config
import torch

def DeNormalizeRGB(image, to8bit=False):
    if image.shape[0] == 3:
        image[0,:,:] *= config.RGB_STD[0]
        image[1,:,:] *= config.RGB_STD[1]
        image[2,:,:] *= config.RGB_STD[2]
        image[0,:,:] += config.RGB_MEAN[0]
        image[1,:,:] += config.RGB_MEAN[1]
        image[2,:,:] += config.RGB_MEAN[2]
    else:
        image[:,:,0] *= config.RGB_STD[0]
        image[:,:,1] *= config.RGB_STD[1]
        image[:,:,2] *= config.RGB_STD[2]
        image[:,:,0] += config.RGB_MEAN[0]
        image[:,:,1] += config.RGB_MEAN[1]
        image[:,:,2] += config.RGB_MEAN[2]
    if to8bit:
        image *= 255
        image = np.floor(image).astype(np.uint8)
    return image

def DeNormalizeRoadWeather(rw_dict):
    for key, value in rw_dict.items():
        rw_dict[key] = value*config.RW_NORM_STD[key] + config.RW_NORM_MEAN[key]
    return rw_dict

def DeNormalizeRange(data):
    data[np.nonzero(data)] *= config.RANGE_STD
    data[np.nonzero(data)] += config.RANGE_MEAN
    return data

def DeNormalizeReflectance(data):
    data[np.nonzero(data)] *= config.REFLECTANCE_STD
    data[np.nonzero(data)] += config.REFLECTANCE_MEAN
    return data

def DeNormalizeRoadTemperature(data):
    return data*config.ROAD_T_STD + ROAD_T_MEAN

def DeNormalizeAirTemperature(data):
    return data*config.AIR_T_STD + AIR_T_MEAN

