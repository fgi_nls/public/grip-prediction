import torch
import numpy as np
import cv2

import config

from scipy.ndimage import gaussian_filter

class RandomSizedCropRotation(object):
    def __init__(self, args, orig_size=None):
        self.scale_min = args.scale_min
        self.scale_max = args.scale_max
        self.rotation_limit = args.rotation_limit
        self.orig_size = orig_size
        self.fixed_size = bool(self.orig_size)
        self.fix_center_bottom = True

    def __call__(self, inputs):
        if not self.fixed_size:
            self.orig_size = inputs['rgb'].shape[0:2]
        scale = np.random.uniform(self.scale_min, self.scale_max) # eq. 1.0 and 2.0
        x_extra = (self.orig_size[1]-1)*(1.0-scale)
        y_extra = (self.orig_size[0]-1)*(1.0-scale)
        if self.fix_center_bottom:
            x_l = x_extra/2
            y_t = y_extra
        else:
            x_l = np.random.uniform(0, x_extra)
            y_t = np.random.uniform(0, y_extra)
        x_r = x_l + (self.orig_size[1]-1)*scale
        y_b = y_t + (self.orig_size[0]-1)*scale
        src_points = np.array([[0, 0], [self.orig_size[1]-1, 0], [self.orig_size[1]-1, self.orig_size[0]-1], [0, self.orig_size[0]-1]], dtype=np.float32)
        dst_points = np.array([[x_l, y_t, 1.0], [x_r, y_t, 1.0], [x_r, y_b, 1.0], [x_l, y_b, 1.0]], dtype=np.float32).T
        
        angle = np.random.uniform(-self.rotation_limit, self.rotation_limit)
        rot_mat = cv2.getRotationMatrix2D(np.array([(x_l+x_r)/2.0, (y_t+y_b)/2.0], dtype=np.float32), angle, 1.0)
        dst_points = np.matmul(rot_mat, dst_points).T
        M = cv2.getAffineTransform(src_points[:-1,:].astype(np.float32), dst_points[:-1,:].astype(np.float32))

        # Test if road weather data is in image area after augmentation
        if 'road_weather' in inputs.keys():
            pix_coords = np.vstack((inputs['road_weather']['pix_x'], inputs['road_weather']['pix_y'], np.ones(len(inputs['road_weather']['pix_x']))))
            pix_coords = np.floor(np.matmul(M, pix_coords).T).astype(int)
            mask = np.logical_and(np.logical_and(pix_coords[:,0] >= 0, pix_coords[:,0] < self.orig_size[1]), np.logical_and(pix_coords[:,1] >= 0, pix_coords[:,1] < self.orig_size[0]))
            if np.sum(mask) == 0 or len(mask) == 0: # if data is outside, cancel augmentation
                return inputs

        for key, data in inputs.items():
            if key == 'range' or key == 'reflectance' or key == 'road_weather': # Pointwise modalities
                pix_coords = np.vstack((data['pix_x'], data['pix_y'], np.ones(len(data['pix_x']))))
                pix_coords = np.floor(np.matmul(M, pix_coords).T).astype(int)
                mask = np.logical_and(np.logical_and(pix_coords[:,0] >= 0, pix_coords[:,0] < self.orig_size[1]), np.logical_and(pix_coords[:,1] >= 0, pix_coords[:,1] < self.orig_size[0]))
                for subkey, subdata in data.items():
                    if subkey == 'pix_x':
                        data[subkey] = pix_coords[:,0][mask]
                    elif subkey == 'pix_y':
                        data[subkey] = pix_coords[:,1][mask]
                    else:
                        data[subkey] = data[subkey][mask]
                inputs[key] = data
            else: # Pixelwise modalities
                #inputs[key] = cv2.warpAffine(data, M, self.orig_size)
                inputs[key] = cv2.warpAffine(data, M, (self.orig_size[1], self.orig_size[0]))
                if key == 'thermal':
                    inputs[key] = inputs[key][:,:,np.newaxis]

        return inputs

class RandomHorizontalFlip(object):
    def __init__(self, args, orig_size=(0,0)):
        self.hor_flip_p = args.horizontal_flipping_p
        self.orig_size = orig_size

    def __call__(self, inputs):
        do_flip = np.random.random() >= 0.5
        if do_flip:
            for key, data in inputs.items():
                if key == 'range' or key == 'reflectance' or key == 'road_weather': # Pointwise modalities
                    data['pix_x'] = self.orig_size[1] - data['pix_x'] - 1
                    inputs[key] = data
                else: # Pixelwise modalities
                    #inputs[key] = inputs[key][:,::-1,:]
                    tmp = np.flip(inputs[key], axis=1).copy()
                    inputs[key] = tmp.copy()
                    del tmp
        return inputs

'''
class RandomHorizontalFlipSemSeg(object):
    def __init__(self, args):
        self.hor_flip_p = args.horizontal_flipping_p

    def __call__(self, image, labels):
        do_flip = np.random.random() >= 0.5
        if do_flip:
            image = image[:,::-1,:]
            labels = labels[:,::-1,:]
        return image, labels
'''

class CropBottom(object):
    def __init__(self, args, orig_size):
        self.crop_amount = args.bottom_crop*config.BOTTOM_CROP
        self.orig_size = orig_size

    def __call__(self, inputs):
        for key, data in inputs.items():
            if key == 'range' or key == 'reflectance' or key == 'road_weather': # Pointwise modalities
                mask = data['pix_y'] < self.orig_size[0] - self.crop_amount
                for subkey, subdata in data.items():
                    data[subkey] = data[subkey][mask]
                inputs[key] = data
            else: # Pixelwise modalities
                #inputs[key] = inputs[key][:-self.crop_amount,:]
                inputs[key] = inputs[key][:self.orig_size[0]-self.crop_amount,:,:]
        return inputs

class CropTo32Pix(object):
    def __init__(self, orig_size=None):
        self.orig_size = orig_size
        if self.orig_size:
            vert_crop = orig_size[0] % 32
            self.top_crop = vert_crop #// 2
            #self.bottom_crop = vert_crop - self.top_crop
            self.bottom_crop = 0 # maximize road area
            hor_crop = orig_size[1] % 32
            self.left_crop = hor_crop // 2
            self.right_crop = hor_crop - self.left_crop

    def __call__(self, inputs):
        if not self.orig_size: # NOTE: if parallelized with varying image sizes, modify to have unique size and crop variables for the procedure
            sx = rgb.shape[1]
            sy = rgb.shape[0]

            vert_crop = sy % 32
            self.top_crop = vert_crop // 2
            self.bottom_crop = vert_crop - top_crop
            hor_crop = sx % 32
            self.left_crop = hor_crop // 2
            self.right_crop = hor_crop - left_crop
        for key, data in inputs.items():
            if key == 'range' or key == 'reflectance' or key == 'road_weather': # Pointwise modalities
                mask = np.logical_and(np.logical_and(data['pix_x'] >= self.left_crop, data['pix_x'] < self.orig_size[1])-self.right_crop, np.logical_and(data['pix_y'] >= self.top_crop, data['pix_y'] < self.orig_size[0]-self.bottom_crop))
                for subkey, subdata in data.items():
                    if subkey == 'pix_x':
                        data[subkey] = data[subkey][mask] - self.left_crop
                    elif subkey == 'pix_y':
                        data[subkey] = data[subkey][mask] - self.top_crop
                    else:
                        data[subkey] = data[subkey][mask]
                inputs[key] = data
            else: # Pixelwise modalities
                inputs[key] = inputs[key][self.top_crop:self.orig_size[0]-self.bottom_crop, self.left_crop:-self.right_crop, :]

        return inputs

class JointBlur(object):
    def __init__(self, kernel_size=7):
        self.kernel_size = kernel_size

    def __call__(self, inputs):
        if 'rgb' in inputs.keys() or 'thermal' in inputs.keys():
            if 'rgb' in inputs.keys() and 'thermal' in inputs.keys():
                image = np.concatenate((inputs['rgb'], inputs['thermal']), axis=2)
            elif 'rgb' in inputs.keys():
                image = inputs['rgb']
            elif 'thermal' in inputs.keys():
                image = inputs['thermal']
            sigma = np.random.uniform(0.1, 2.0)
            #image = gaussian_filter(image, sigma, radius=self.kernel_size, axes=(0,1))
            image = gaussian_filter(image, sigma, axes=(0,1))
            #for i in range(image.shape[2]):
                #image[:,:,i:(i+1)] = gaussian_filter(image[:,:,i:(i+1)], sigma)
            if 'rgb' in inputs.keys() and 'thermal' in inputs.keys():
                inputs['rgb'] = image[:,:,0:3].astype(np.uint8)
                inputs['thermal'] = image[:,:,3:]
            elif 'rgb' in inputs.keys():
                inputs['rgb'] = image.astype(np.uint8)
            elif 'thermal' in inputs.keys():
                inputs['thermal'] = image

        return inputs


'''
class CropTo32PixSemSeg(object):
    def __call__(self, rgb, labels):
        assert rgb.shape[:2] == labels.shape[:2]
        sx = rgb.shape[1]
        sy = rgb.shape[0]

        vert_crop = sy % 32
        top_crop = vert_crop // 2
        bottom_crop = vert_crop - top_crop
        hor_crop = sx % 32
        left_crop = hor_crop // 2
        right_crop = hor_crop - left_crop

        rgb = rgb[top_crop:-bottom_crop, left_crop:-right_crop, :]
        labels = labels[top_crop:-bottom_crop, left_crop:-right_crop, :]
        return rgb, labels
'''

