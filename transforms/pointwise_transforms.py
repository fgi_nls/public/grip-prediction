import numpy as np
import config
import torch

class PointwiseNoise(object):
    def __init__(self, modality, noise_max):
        self.modality = modality
        self.noise_max = noise_max

    def __call__(self, sample_dict):
        noise_amount = np.random.uniform(0.0, noise_max)
        sample_dict[self.modality] += np.random.normal(scale=noise_amount, size=sample_dict[self.modality].shape)
        return sample_dict

class PointwiseNormalization(object):
    def __init__(self, modality, mean, std):
        self.modality = modality
        self.mean = mean
        self.std = std

    def __call__(self, sample_dict):
        sample_dict[self.modality] = (sample_dict[self.modality] - self.mean)/self.std
        return sample_dict

class RoadWeatherNormalization(object):
    def __init__(self):
        pass

    def __call__(self, sample_dict):
        for modality in sample_dict.keys():
            if not (modality == 'pix_x' or modality == 'pix_y' or modality == 'weight'):
                # Clip layer values to maximum value
                if modality in ['Water', 'Ice', 'Snow']:
                    sample_dict[modality] = np.clip(sample_dict[modality], 0, config.RW_CLIP_VALS[modality])
                # Normalize the value
                sample_dict[modality] = (sample_dict[modality] - config.RW_NORM_MEAN[modality])/config.RW_NORM_STD[modality]
        return sample_dict

class PointwiseScaleAndBias(object):
    def __init__(self, modality, scale_min=0.9, scale_max=1.1, bias_min=-0.05, bias_max=0.05):
        self.modality = modality
        self.scale_min = scale_min
        self.scale_max = scale_max
        self.bias_min = bias_min
        self.bias_max = bias_max

    def __call__(self, sample_dict):
        scale = np.random.uniform(self.scale_min, self.scale_max)
        bias = np.random.uniform(self.bias_min, self.bias_max)
        sample_dict[self.modality] = sample_dict[self.modality]*scale + bias
        return sample_dict

class PointwiseToTensor(object):
    ''' Currently does not transform pixel coordinates '''
    def __init__(self):
        pass

    def __call__(self, sample_dict):
        for modality in sample_dict.keys():
            #if not (modality == 'pix_x' or modality == 'pix_y'): # NOTE: possible that also these should be tensors
            sample_dict[modality] = torch.from_numpy(sample_dict[modality])
        return sample_dict

class PointwiseToImage(object):
    def __init__(self, target_size, channels = 1):
        self.target_size = target_size + (channels,)

    def __call__(self, sample_dict):
        if 'range' in sample_dict.keys():
            image = config.RANGE_ZERO*np.ones(self.target_size, dtype=np.float32)
        elif 'reflectance' in sample_dict.keys():
            image = config.REFLECTANCE_ZERO*np.ones(self.target_size, dtype=np.float32)
        else:
            image = np.zeros(self.target_size, dtype=np.float32)
        mask = np.logical_and(np.logical_and(0 <= sample_dict['pix_y'], sample_dict['pix_y'] < self.target_size[0]), np.logical_and(0 <= sample_dict['pix_x'], sample_dict['pix_x'] < self.target_size[1]))
        for modality in sample_dict.keys():
            if not (modality == 'pix_x' or modality == 'pix_y'):
                image[sample_dict['pix_y'][mask], sample_dict['pix_x'][mask]] = sample_dict[modality][mask][:,np.newaxis]
        return image

class PointwiseMakeUnique(object):
    ''' In case some measurements are in the same pixel, make them unique '''
    def __init__(self, method='mean'):
        self.method = method

    def __call__(self, sample_dict):
        points = np.stack((sample_dict['pix_x'], sample_dict['pix_y']), axis=1)
        new_points, index, inverse = np.unique(points, axis=0, return_index=True, return_inverse=True)
        #new_points, inverse = np.unique(points, axis=0, return_inverse=True)
        if self.method == 'mean':
            for modality in sample_dict.keys():
                if not (modality == 'pix_x' or modality == 'pix_y'):
                    new_array = np.zeros(len(new_points), dtype=sample_dict[modality].dtype)
                    N_array = np.zeros(len(new_points))
                    for i, k in enumerate(inverse):
                        new_array[k] += sample_dict[modality][i]
                        N_array[k] += 1
                    sample_dict[modality] = new_array / N_array
                else: # for pix_x and pix_y
                    sample_dict[modality] = sample_dict[modality][index]
        if self.method == 'other':
            for modality in sample_dict.keys():
                if not (modality == 'pix_x' or modality == 'pix_y'):
                    sample_dict[modality] = sample_dict[modality][index]
                else: # for pix_x and pix_y
                    sample_dict[modality] = sample_dict[modality][index]
        
        return sample_dict

