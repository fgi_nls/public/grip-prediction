import numpy as np
import config

class NormalizeTemperature(object):
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std
    def __call__(self, t):
        return (t - self.mean)/self.std

