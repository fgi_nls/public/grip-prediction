import numpy as np
import scipy.ndimage
from PIL import Image, ImageDraw
import config

class NormalizeSample(object):
    def __init__(self):
        pass
    def __call__(self, image):
        mean = np.mean(image[image != 0])
        std = np.std(image[image != 0])
        image[image != 0] = (image[image != 0] - mean)/std
        return image

class NormalizeFromRoadArea(object):
    '''
    Normlizes a thermal camera image with respect to a road area sample defined
    with a polygon. Calculates mean and std excluding highest values in the area
    and normalizes wrt road if road area std > 1.0*whole image std
    '''
    def __init__(self):
        polygon = [(671,455), (714,189), (1104,189), (1147,455)]
        img = Image.new('L', (config.ORIG_IMAGE_SIZE[1], config.ORIG_IMAGE_SIZE[0]), 0)
        ImageDraw.Draw(img).polygon(polygon, outline=1, fill=1)
        self.mask = np.array(img)
        self.mask = self.mask[:,:,np.newaxis].astype(bool)

    def __call__(self, image):
        full_mean = np.mean(image[image != 0])
        full_std = np.std(image[image != 0])
        full_mask = np.logical_and(image != 0, image < (full_mean + full_std))
        #full_mean = np.mean(image[full_mask])
        full_std = np.std(image[full_mask])

        road_mean = np.mean(image[self.mask])
        road_std = np.std(image[self.mask])
        road_mask = np.logical_and(self.mask, image < (road_mean + road_std))
        road_mean = np.mean(image[road_mask]) # NOTE, is this needed?
        road_std = np.std(image[road_mask])
        #print(road_mean, road_std, full_mean, full_std)
        if road_std > 0.1*full_std:
            image[image != 0] = (image[image != 0] - road_mean)/road_std
            #print('road')
        else:
            image[image != 0] = (image[image != 0] - road_mean)/full_std
            #print('full')
        return image

class MedianFilter(object):
    def __init__(self, kernel_size):
        self.kernel_size = kernel_size

    def __call__(self, image):
        return scipy.ndimage.median_filter(image, size=self.kernel_size)

class RandomScaleAndBias(object):
    '''
    Assumes input image is already normalized to zero mean and unit variance.
    '''
    def __init__(self, scale_min=0.5, scale_max=2.0, bias_min=-0.2, bias_max=0.2):
        self.scale_min = scale_min
        self.scale_max = scale_max
        self.bias_min = bias_min
        self.bias_max = bias_max

    def __call__(self, image):
        scale = np.random.uniform(self.scale_min, self.scale_max)
        bias = np.random.uniform(self.bias_min, self.bias_max)
        return image*scale + bias

