from numpy import random

class ProbabilityWrapper(object):
    def __init__(self, p, transform):
        self.p = p
        self.transform = transform
    def __call__(self, data):
        if self.p == 1 or random.random() <= self.p:
            return self.transform(data)
        return data

