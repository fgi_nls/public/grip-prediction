import torch
from torch.utils import data as torch_data
import numpy as np
import os
import cv2
import pandas as pd
#import PIL
#import tifffile

from utils.range_image_file import ReadRangeImage
from utils.rw_image_file import ReadRWImage

import torchvision.transforms as torch_transforms
import transforms.joint_transforms as joint_transforms
import transforms.thermal_transforms as thermal_transforms
import transforms.pointwise_transforms as pointwise_transforms
import transforms.temperature_transforms as temperature_transforms
import transforms.transform_utils as t_utils

import config

import time

class DemoSet(torch_data.Dataset):
    def __init__(self, mode='Test', dir='data', inputs=['rgb']):
        self.main_folder = dir #config.FGIMULTIMODAL_FOLDER
        # first letter uppercase, also add lower version
        self.mode = mode.capitalize()
        self.mode_u = mode.lower()
        #self.subfolders = os.listdir(self.main_folder)

        self.inputs = inputs
        self.outputs = ['Grip', 'Water', 'Ice', 'Snow']
        self.modalities = self.inputs + self.outputs

        self.rw_datas = config.RW_DATAS
        self.rw_weight_type = 'linear' # add to args if needed

        # Find dataset samples
        self.samples = [s[:-4] for s in sorted(os.listdir(os.path.join(self.main_folder, self.mode, "Camera")))]

        #self.samples = np.array(self.samples)[df['N'].values >= config.MIN_RW_POINTS]
        # Read sample weighting csv
        self.weights = []
        # Save len
        self.len = len(self.samples)

        # Initialize transforms
        self.joint_transforms = []
        self.rgb_transforms = []
        self.thermal_normalization = []
        self.thermal_transforms = []
        self.range_transforms = []
        self.reflectance_transforms = []
        self.rw_transforms = []
        self.road_t_transforms = []
        self.air_t_transforms = []
        self.weight_transforms = []
        self.id_transforms = []

        # Joint transforms for all modalities
        new_size = config.ORIG_IMAGE_SIZE
        self.joint_transforms.append(joint_transforms.CropTo32Pix(new_size))
        new_size = (new_size[0] - new_size[0] % 32, new_size[1] - new_size[1] % 32)
        self.new_size = new_size
        self.joint_transforms = torch_transforms.Compose(self.joint_transforms)

        # Transforms for single modalities
        if 'rgb' in self.modalities:
            self.rgb_transforms.append(torch_transforms.ToTensor())
            self.rgb_transforms.append(torch_transforms.Normalize(config.RGB_MEAN, config.RGB_STD))
            self.rgb_transforms = torch_transforms.Compose(self.rgb_transforms)

        if 'thermal' in self.modalities:
            self.thermal_normalization.append(thermal_transforms.NormalizeSample())
            self.thermal_normalization = torch_transforms.Compose(self.thermal_normalization)
            self.thermal_transforms.append(torch_transforms.ToTensor())
            self.thermal_transforms = torch_transforms.Compose(self.thermal_transforms)

        if 'reflectance' in self.modalities:
            self.reflectance_transforms.append(pointwise_transforms.PointwiseMakeUnique()) # TODO: check if needed or moved to preprocessing
            self.reflectance_transforms.append(pointwise_transforms.PointwiseNormalization('reflectance', config.ROAD_REFLECTANCE_MEAN, config.ROAD_REFLECTANCE_STD))
            self.reflectance_transforms.append(pointwise_transforms.PointwiseToImage(self.new_size))
            self.reflectance_transforms.append(torch_transforms.ToTensor())
            self.reflectance_transforms = torch_transforms.Compose(self.reflectance_transforms)

        if 'road_weather' in self.modalities:
            self.rw_transforms.append(pointwise_transforms.PointwiseMakeUnique()) # TODO: check if needed or moved to preprocessing
            self.rw_transforms.append(pointwise_transforms.RoadWeatherNormalization())
            self.rw_transforms.append(pointwise_transforms.PointwiseToTensor())
            self.rw_transforms = torch_transforms.Compose(self.rw_transforms)

        self.id_transforms.append(torch_transforms.ToTensor())
        self.id_transforms = torch_transforms.Compose(self.id_transforms)

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        modalities, scalar_mods = self.get_modalities(idx)
        if 'thermal' in self.modalities:
            modalities['thermal'] = self.thermal_normalization(modalities['thermal'])
        modalities = self.joint_transforms(modalities)

        # apply transforms
        if 'rgb' in self.modalities:
            modalities['rgb'] = self.rgb_transforms(modalities['rgb'])
        start_time = time.time()
        if 'thermal' in self.modalities:
            modalities['thermal'] = self.thermal_transforms(modalities['thermal'])
        if 'range' in self.modalities:
            modalities['range'] = self.range_transforms(modalities['range'])
        if 'reflectance' in self.modalities:
            modalities['reflectance'] = self.reflectance_transforms(modalities['reflectance'])
        if 'road_weather' in self.modalities:
            modalities['road_weather'] = self.rw_transforms(modalities['road_weather'])
        if 'road_t' in self.modalities:
            modalities['road_t'] = self.road_t_transforms(scalar_mods['road_t'])
        if 'air_t' in self.modalities:
            modalities['air_t'] = self.air_t_transforms(scalar_mods['air_t'])
        if len(self.weights) != 0:
            modalities['weight'] = self.weight_transforms(scalar_mods['weight'])
        if 'img_id' in self.modalities:
            modalities['img_id'] = self.id_transforms(scalar_mods['img_id'])
        
        return modalities
        '''
        inputs = {}
        for mod in self.inputs:
            inputs[mod] = modalities[mod]
        outputs = {}
        for mod in self.outputs:
            outputs[mod] = modalities[mod]

        return inputs, outputs
        '''

    def get_modalities(self, idx):
        ret = {}
        scalar_mods = {}

        if 'rgb' in self.modalities:
            image = cv2.imread(os.path.join(self.main_folder, self.mode, "Camera", self.samples[idx]) + '.jpg')
            #image = PIL.imread(os.path.join(self.main_folder, self.samples[idx][0], "Camera", self.samples[idx][1]) + '.png')
            #image = image.astype(np.float32)
            ret['rgb'] = image[:,:,::-1].copy() # bgr to rgb
        if 'thermal' in self.modalities:
            #image = tifffile.imread(os.path.join(self.main_folder, self.samples[idx][0], "Thermal_camera", self.samples[idx][1]) + '.tiff')
            image = cv2.imread(os.path.join(self.main_folder, self.mode, "Thermal_camera", self.samples[idx]) + '.png', cv2.IMREAD_ANYDEPTH)
            image = image.astype(np.float32)
            image = image[:,:,np.newaxis]
            ret['thermal'] = image
        if 'range' in self.modalities or 'reflectance' in self.modalities:
            num_rows, num_cols, pix_x, pix_y, ranges, reflectances = ReadRangeImage(os.path.join(self.main_folder, self.mode, "Lidar", self.samples[idx]) + '.rg')
            if 'range' in self.modalities:
                ret['range'] = {'pix_x': pix_x.astype(np.short), 'pix_y': pix_y.astype(np.short), 'range': ranges.astype(np.float32)}
            if 'reflectance' in self.modalities:
                ret['reflectance'] = {'pix_x': pix_x.astype(np.short), 'pix_y': pix_y.astype(np.short), 'reflectance': reflectances.astype(np.float32)}
        if 'road_weather' in self.modalities:
            pix_x, pix_y, rw_datas = ReadRWImage(os.path.join(self.main_folder, self.mode, "RoadWeather", self.samples[idx]) + '.rw', out_datas=self.rw_datas)
            ret['road_weather'] = {'pix_x': pix_x.astype(np.short), 'pix_y': pix_y.astype(np.short)}
            #ret['road_weather']['pix'] = (pix_y.astype(np.int_), pix_x.astype(np.int_))
            for i, key in enumerate(self.rw_datas):
                ret['road_weather'][key] = rw_datas[i].astype(np.float32)
        if 'img_id' in self.modalities:
            scalar_mods['img_id'] = np.array(np.array(self.samples[idx]), ndmin=2, dtype=int)

        return ret, scalar_mods

PAD_VALUE_PIX = -1
