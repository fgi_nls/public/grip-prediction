import torch
from torch.utils import data as torch_data
import numpy as np
import os
import cv2
import pandas as pd
#import PIL
#import tifffile

from utils.range_image_file import ReadRangeImage
from utils.rw_image_file import ReadRWImage

import torchvision.transforms as torch_transforms
import transforms.joint_transforms as joint_transforms
import transforms.thermal_transforms as thermal_transforms
import transforms.pointwise_transforms as pointwise_transforms
import transforms.temperature_transforms as temperature_transforms
import transforms.transform_utils as t_utils

import config

import time

class FGIMultimodalDataset(torch_data.Dataset):
    def __init__(self, args, mode='Train', worst_list=[]):
        self.main_folder = args.data_dir #config.FGIMULTIMODAL_FOLDER
        # first letter uppercase, also add lower version
        self.mode = mode.capitalize()
        self.mode_u = mode.lower()
        #self.subfolders = os.listdir(self.main_folder)

        self.inputs = args.multimodal_inputs
        self.outputs = args.multimodal_outputs
        self.modalities = self.inputs + self.outputs

        self.rw_datas = config.RW_DATAS
        self.rw_weight_type = 'linear' # add to args if needed

        # Find dataset samples
        if mode == 'worst_cases':
            self.samples = [os.path.join(self.main_folder, self.mode, "Camera", s)[:-4] for s in worst_list]
        else:
            self.samples = [s[:-4] for s in sorted(os.listdir(os.path.join(self.main_folder, self.mode, "Camera")))]

        # find samples with no rw points in them, exclude them
        if args.fix_rw_N:
            df = pd.read_csv(os.path.join(self.main_folder, self.mode_u + '_N_road_weather_measurements_unique.csv'))
            sample_mask = df['N_unique'].values >= config.MIN_RW_UNIQUE_POINTS
            #print("Used value for config.MIN_RW_UNIQUE_POINTS:", config.MIN_RW_UNIQUE_POINTS)
        else:
            #df = pd.read_csv(os.path.join(self.main_folder, self.mode_u + '_N_road_weather_measurements.csv'))
            df = pd.read_csv(os.path.join(self.main_folder, self.mode_u + '_N_road_weather_measurements_unique.csv'))
            sample_mask = df['N_unique'].values >= config.MIN_RW_POINTS
        assert self.samples == [str(s) for s in df['Timestamp'].values]
        #self.samples = np.array(self.samples)[df['N'].values >= config.MIN_RW_POINTS]
        # Read sample weighting csv
        if not args.no_weighting:
            if mode == 'Val' and args.weighted_validation:
                weight_df = pd.read_csv(config.VAL_WEIGHTS) # "validation_weights.csv"
            elif mode == 'Test' and args.weighted_validation:
                weight_df = pd.read_csv(config.TEST_WEIGHTS) # "testing_weights.csv"
            else: # mode == 'Train' by default
                weight_df = pd.read_csv(config.TRAIN_WEIGHTS) # "sampling_weights.csv"

            weight_ts = weight_df['Timestamp'].values
            weights = weight_df['weights'].values

            # When using SlipperinessMini dataset with 100 samples, fix weights
            if len(self.samples) <= 100:
                weight_ts = weight_ts[:len(self.samples)]
                weights =  weights[:len(self.samples)]

            # Filter training set points based on min rw points and set weights to empty array
            # if the set is for validation or testing and weights aren't used
            if mode == 'Train':
                assert sorted([str(s) for s in weight_ts]) == sorted(self.samples)
                assert [str(s) for s in weight_ts] == self.samples
                if args.full_weighting:
                    self.weights = np.array(weights[sample_mask])
                elif args.no_weighting:
                    self.weights = np.array(weights[sample_mask])
                    self.weights.fill(1.0)
                elif args.sqrt_weighting:
                    self.weights = np.sqrt(np.array(weights[sample_mask])/10.0)
                else:
                    self.weights = np.sqrt(np.array(weights[sample_mask]))
            elif (mode == 'Val' or mode == 'Test') and args.weighted_validation:
                self.weights = np.array(weights[sample_mask]) # duplicated, left for further reference
                #self.weights = weights
        else:
            self.weights = []

        if 'road_t' in self.modalities or 'air_t' in self.modalities:
            df_t = pd.read_csv(os.path.join(self.main_folder, self.mode_u + '_temperatures.csv'))
            assert self.samples == [str(s) for s in df_t['Timestamp'].values]
            self.road_t_data = df_t['SurfaceT'].values[sample_mask].astype(np.float32)
            self.air_t_data = df_t['AirT'].values[sample_mask].astype(np.float32)

        # Exclude bad samples
        #print(len(self.samples), np.sum(df['N'].values >= 2), np.sum(df['N'].values >= 10))
        self.samples = np.array(self.samples)[sample_mask]
        print(len(self.samples), "samples in dataset", mode)

        # When using SlipperinessMini dataset with 100 samples, repeat data
        if len(self.samples) <= 100:
            if self.mode == 'Train':
                self.samples = np.repeat(self.samples, 70)
                self.weights = np.repeat(self.weights, 70)
            else: # Val or test
                self.samples = np.repeat(self.samples, 3)
                self.weights = np.repeat(self.weights, 3)

        # Save len
        self.len = len(self.samples)

        # Initialize transforms
        self.joint_transforms = []
        self.rgb_transforms = []
        self.thermal_normalization = []
        self.thermal_transforms = []
        self.range_transforms = []
        self.reflectance_transforms = []
        self.rw_transforms = []
        self.road_t_transforms = []
        self.air_t_transforms = []
        self.weight_transforms = []
        self.id_transforms = []

        # Joint transforms for all modalities
        self.joint_transforms.append(joint_transforms.CropBottom(args, config.ORIG_IMAGE_SIZE))
        new_size = (config.ORIG_IMAGE_SIZE[0] - args.bottom_crop*config.BOTTOM_CROP, config.ORIG_IMAGE_SIZE[1])
        self.joint_transforms.append(joint_transforms.CropTo32Pix(new_size))
        new_size = (new_size[0] - new_size[0] % 32, new_size[1] - new_size[1] % 32)
        self.new_size = new_size
        if self.mode == 'Train':
            if args.gblur:
                self.joint_transforms.append(t_utils.ProbabilityWrapper(args.blur_p, joint_transforms.JointBlur(7)))
            if args.scale_min != 1.0 or args.scale_max != 1.0 or args.rotation_limit:
                self.joint_transforms.append(t_utils.ProbabilityWrapper(args.scalerot_p, joint_transforms.RandomSizedCropRotation(args, new_size)))
            if args.horizontal_flipping_p:
                self.joint_transforms.append(joint_transforms.RandomHorizontalFlip(args, new_size))
        self.joint_transforms = torch_transforms.Compose(self.joint_transforms)

        # Transforms for single modalities
        if 'rgb' in self.modalities:
            self.rgb_transforms.append(torch_transforms.ToTensor())
            if self.mode == 'Train':
                if args.brightness_j or args.contrast_j or args.saturation_j or args.hue_j:
                    self.rgb_transforms.append(t_utils.ProbabilityWrapper(args.colorjitter_p,
                                               torch_transforms.ColorJitter(brightness = args.brightness_j,
                                                                            contrast = args.contrast_j,
                                                                            saturation = args.saturation_j,
                                                                            hue = args.hue_j)))
                #if args.gblur:
                    #self.rgb_transforms.append(t_utils.ProbabilityWrapper(args.blur_p, torch_transforms.GaussianBlur(7))) # 7 is only kernel_size, actual blur happens with 0.1-2.0 variance in pixel units
            self.rgb_transforms.append(torch_transforms.Normalize(config.RGB_MEAN, config.RGB_STD))
            self.rgb_transforms = torch_transforms.Compose(self.rgb_transforms)

        if 'thermal' in self.modalities:
            if args.thermal_normalization == 'sample':
                self.thermal_normalization.append(thermal_transforms.NormalizeSample())
            elif args.thermal_normalization == 'road':
                self.thermal_normalization.append(thermal_transforms.NormalizeFromRoadArea())
            else: # PLACEHOLDER FOR OTHER NORMALIZATIONS
                self.thermal_normalization.append(thermal_transforms.NormalizeSample())
            self.thermal_normalization = torch_transforms.Compose(self.thermal_normalization)
            if args.thermal_median_filter != 1:
                self.thermal_transforms.append(thermal_transforms.MedianFilter(args.thermal_median_filter))
            if self.mode == 'Train':
                if len(args.thermal_scale_and_bias):
                    scale_min, scale_max, bias_min, bias_max = args.thermal_scale_and_bias
                    self.thermal_transforms.append(thermal_transforms.RandomScaleAndBias(scale_min=scale_min, scale_max=scale_max, bias_min=bias_min, bias_max=bias_max))
                #if args.thermal_blur:
                    #self.thermal_transforms.append(t_utils.ProbabilityWrapper(args.tblur_p, torch_transforms.GaussianBlur(7)))
            self.thermal_transforms.append(torch_transforms.ToTensor())
            self.thermal_transforms = torch_transforms.Compose(self.thermal_transforms)

        if 'range' in self.modalities:
            self.range_transforms.append(pointwise_transforms.PointwiseMakeUnique()) # TODO: check if needed or moved to preprocessing
            if self.mode == 'Train':
                if args.range_noise: # TODO
                    self.range_transforms.append(pointwise_transforms.PointwiseNoise('range', args.range_noise))
            if args.road_lidar_normalization:
                self.range_transforms.append(pointwise_transforms.PointwiseNormalization('range', config.ROAD_RANGE_MEAN, config.ROAD_RANGE_STD))
            else:
                self.range_transforms.append(pointwise_transforms.PointwiseNormalization('range', config.RANGE_MEAN, config.RANGE_STD))
            if self.mode == 'Train':
                if len(args.range_scale_and_bias):
                    scale_min, scale_max, bias_min, bias_max = args.range_scale_and_bias
                    self.range_transforms.append(pointwise_transforms.PointwiseScaleAndBias('range', scale_min=scale_min, scale_max=scale_max, bias_min=bias_min, bias_max=bias_max))
            #self.range_transforms.append(pointwise_transforms.PointwiseToTensor())
            self.range_transforms.append(pointwise_transforms.PointwiseToImage(self.new_size))
            self.range_transforms.append(torch_transforms.ToTensor())
            self.range_transforms = torch_transforms.Compose(self.range_transforms)

        if 'reflectance' in self.modalities:
            self.reflectance_transforms.append(pointwise_transforms.PointwiseMakeUnique()) # TODO: check if needed or moved to preprocessing
            if self.mode == 'Train':
                if args.reflectance_noise: # TODO
                    self.reflectance_transforms.append(pointwise_transforms.PointwiseNoise('reflectance', args.reflectance_noise))
            if args.road_lidar_normalization:
                self.reflectance_transforms.append(pointwise_transforms.PointwiseNormalization('reflectance', config.ROAD_REFLECTANCE_MEAN, config.ROAD_REFLECTANCE_STD))
            else:
                self.reflectance_transforms.append(pointwise_transforms.PointwiseNormalization('reflectance', config.REFLECTANCE_MEAN, config.REFLECTANCE_STD))
            if self.mode == 'Train':
                if len(args.reflectance_scale_and_bias):
                    scale_min, scale_max, bias_min, bias_max = args.reflectance_scale_and_bias
                    self.reflectance_transforms.append(pointwise_transforms.PointwiseScaleAndBias('reflectance', scale_min=scale_min, scale_max=scale_max, bias_min=bias_min, bias_max=bias_max))
            #self.reflectance_transforms.append(pointwise_transforms.PointwiseToTensor())
            self.reflectance_transforms.append(pointwise_transforms.PointwiseToImage(self.new_size))
            self.reflectance_transforms.append(torch_transforms.ToTensor())
            self.reflectance_transforms = torch_transforms.Compose(self.reflectance_transforms)

        if 'road_weather' in self.modalities:
            self.rw_transforms.append(pointwise_transforms.PointwiseMakeUnique()) # TODO: check if needed or moved to preprocessing
            self.rw_transforms.append(pointwise_transforms.RoadWeatherNormalization())
            self.rw_transforms.append(pointwise_transforms.PointwiseToTensor())
            self.rw_transforms = torch_transforms.Compose(self.rw_transforms)

        if 'road_t' in self.modalities:
            self.road_t_transforms.append(temperature_transforms.NormalizeTemperature(config.ROAD_T_MEAN, config.ROAD_T_STD))
            self.road_t_transforms.append(torch_transforms.ToTensor())
            self.road_t_transforms = torch_transforms.Compose(self.road_t_transforms)
        if 'air_t' in self.modalities:
            self.air_t_transforms.append(temperature_transforms.NormalizeTemperature(config.AIR_T_MEAN, config.AIR_T_STD))
            self.air_t_transforms.append(torch_transforms.ToTensor())
            self.air_t_transforms = torch_transforms.Compose(self.air_t_transforms)
        if len(self.weights) != 0:
            self.weight_transforms.append(torch_transforms.ToTensor())
            self.weight_transforms = torch_transforms.Compose(self.weight_transforms)
        self.id_transforms.append(torch_transforms.ToTensor())
        self.id_transforms = torch_transforms.Compose(self.id_transforms)

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        modalities, scalar_mods = self.get_modalities(idx)
        if 'thermal' in self.modalities:
            modalities['thermal'] = self.thermal_normalization(modalities['thermal'])
        modalities = self.joint_transforms(modalities)

        # apply transforms
        if 'rgb' in self.modalities:
            modalities['rgb'] = self.rgb_transforms(modalities['rgb'])
        start_time = time.time()
        if 'thermal' in self.modalities:
            modalities['thermal'] = self.thermal_transforms(modalities['thermal'])
        if 'range' in self.modalities:
            modalities['range'] = self.range_transforms(modalities['range'])
        if 'reflectance' in self.modalities:
            modalities['reflectance'] = self.reflectance_transforms(modalities['reflectance'])
        if 'road_weather' in self.modalities:
            modalities['road_weather'] = self.rw_transforms(modalities['road_weather'])
        if 'road_t' in self.modalities:
            modalities['road_t'] = self.road_t_transforms(scalar_mods['road_t'])
        if 'air_t' in self.modalities:
            modalities['air_t'] = self.air_t_transforms(scalar_mods['air_t'])
        if len(self.weights) != 0:
            modalities['weight'] = self.weight_transforms(scalar_mods['weight'])
        if 'img_id' in self.modalities:
            modalities['img_id'] = self.id_transforms(scalar_mods['img_id'])
        
        return modalities
        '''
        inputs = {}
        for mod in self.inputs:
            inputs[mod] = modalities[mod]
        outputs = {}
        for mod in self.outputs:
            outputs[mod] = modalities[mod]

        return inputs, outputs
        '''

    def get_modalities(self, idx):
        ret = {}
        scalar_mods = {}

        if 'rgb' in self.modalities:
            image = cv2.imread(os.path.join(self.main_folder, self.mode, "Camera", self.samples[idx]) + '.jpg')
            #image = PIL.imread(os.path.join(self.main_folder, self.samples[idx][0], "Camera", self.samples[idx][1]) + '.png')
            #image = image.astype(np.float32)
            ret['rgb'] = image[:,:,::-1].copy() # bgr to rgb
        if 'thermal' in self.modalities:
            #image = tifffile.imread(os.path.join(self.main_folder, self.samples[idx][0], "Thermal_camera", self.samples[idx][1]) + '.tiff')
            image = cv2.imread(os.path.join(self.main_folder, self.mode, "Thermal_camera", self.samples[idx]) + '.png', cv2.IMREAD_ANYDEPTH)
            image = image.astype(np.float32)
            image = image[:,:,np.newaxis]
            ret['thermal'] = image
        if 'range' in self.modalities or 'reflectance' in self.modalities:
            num_rows, num_cols, pix_x, pix_y, ranges, reflectances = ReadRangeImage(os.path.join(self.main_folder, self.mode, "Lidar", self.samples[idx]) + '.rg')
            if 'range' in self.modalities:
                ret['range'] = {'pix_x': pix_x.astype(np.short), 'pix_y': pix_y.astype(np.short), 'range': ranges.astype(np.float32)}
            if 'reflectance' in self.modalities:
                ret['reflectance'] = {'pix_x': pix_x.astype(np.short), 'pix_y': pix_y.astype(np.short), 'reflectance': reflectances.astype(np.float32)}
        if 'road_weather' in self.modalities:
            pix_x, pix_y, rw_datas = ReadRWImage(os.path.join(self.main_folder, self.mode, "RoadWeather", self.samples[idx]) + '.rw', out_datas=self.rw_datas)
            ret['road_weather'] = {'pix_x': pix_x.astype(np.short), 'pix_y': pix_y.astype(np.short)}
            #ret['road_weather']['pix'] = (pix_y.astype(np.int_), pix_x.astype(np.int_))
            for i, key in enumerate(self.rw_datas):
                ret['road_weather'][key] = rw_datas[i].astype(np.float32)
            # TODO: add measurement point weights as new rw key
            if self.rw_weight_type == 'linear':
                weights = (np.clip(pix_y - config.HORIZON_LEVEL_PIX, 0, self.new_size[0]+10)/(1.0*self.new_size[0])).astype(np.float32)
            else:
                weights = np.ones(len(pix_y), dtype=np.float32)
            ret['road_weather']['weight'] = weights/np.mean(weights)
            #print('test', ret['road_weather']['weight'])
        if 'road_t' in self.modalities:
            scalar_mods['road_t'] = np.array(self.road_t_data[idx], ndmin=2, dtype=np.float32)
        if 'air_t' in self.modalities:
            scalar_mods['air_t'] = np.array(self.air_t_data[idx], ndmin=2, dtype=np.float32)
        if len(self.weights) != 0:
            scalar_mods['weight'] = np.array((self.len/10)*self.weights[idx], ndmin=2, dtype=np.float32)
        if 'img_id' in self.modalities:
            scalar_mods['img_id'] = np.array(np.array(self.samples[idx]), ndmin=2, dtype=int)

        return ret, scalar_mods
    
    def GetWeights(self):
        return self.weights

PAD_VALUE_PIX = -1

def CollateFGIData(data):
    keys = data[0].keys()
    ret = {}
    for k in keys:
        if k == 'rgb' or k == 'thermal' or k == 'range' or k == 'reflectance':
            r = torch.zeros((len(data),) + data[0][k].shape, dtype=data[0][k].dtype)
            for i, d in enumerate(data):
                r[i] = d[k]
            ret[k] = r
        elif k == 'road_t' or k == 'air_t' or k == 'weight':
            r = torch.zeros((len(data), 1), dtype=data[0][k].dtype)
            for i, d in enumerate(data):
                r[i] = d[k]
            ret[k] = r
        elif k == 'img_id':
            r = torch.zeros((len(data), 1), dtype=data[0][k].dtype)
            for i, d in enumerate(data):
                r[i] = d[k]
            ret[k] = r
        else: # pixel coordinate modalities
            subret = {}
            max_len = max([len(data[i][k]['pix_x']) for i in range(len(data))])
            for subk in data[0][k].keys():
                sub_r = PAD_VALUE_PIX*torch.ones((len(data), max_len), dtype=data[0][k][subk].dtype)
                for i, d in enumerate(data):
                    sub_r[i,:len(d[k][subk])] = d[k][subk]
                subret[subk] = sub_r
            ret[k] = subret

    return ret

