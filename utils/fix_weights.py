import torch
from collections import OrderedDict

def load_fixed_weights(model, weight_path, device='cpu'):
    weights = torch.load(weight_path, map_location=device)
    fixed_weights = OrderedDict()
    for k, v in weights.items():
        name = k[7:] # remove `module.`
        fixed_weights[name] = v

    model.load_state_dict(fixed_weights)

    return model