import torch
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
import torchvision.transforms as transforms
import matplotlib as mpl
import matplotlib.cm as cm
import config

mean = config.RGB_MEAN
std = config.RGB_STD

def plot_image(batch, outputs, notebook=True, fig=None, idx=0):

    norm = mpl.colors.Normalize(vmin=0.1, vmax=0.82)
    cmap = cm.viridis
    m = cm.ScalarMappable(norm=norm, cmap=cmap)

    inv_normalize = transforms.Normalize(
                    mean= [-m/s for m, s in zip(mean, std)],
                    std= [1/s for s in std]
                    )
    
    label_outputs = outputs[:,0,:,:]
    grip = label_outputs*config.RW_NORM_STD['Grip']+config.RW_NORM_MEAN['Grip']

    img = np.transpose(inv_normalize(batch['rgb'][0]).numpy(force=True), [1,2,0])
    gray = np.zeros((img.shape[0], img.shape[1], 3))
    gray[:,:,0] = np.dot(img[...,:3], [0.2989, 0.5870, 0.1140])
    gray[:,:,1] = np.dot(img[...,:3], [0.2989, 0.5870, 0.1140])
    gray[:,:,2] = np.dot(img[...,:3], [0.2989, 0.5870, 0.1140])

    if torch.is_tensor(grip):
        grip = grip.numpy(force=True)

    color_arr = m.to_rgba(grip.reshape((512, 1824)), alpha=False)
    gray_and_color = 0.5*gray + 0.5*color_arr[:,:,:3]
    pil_out = Image.fromarray((gray_and_color*255).astype(np.uint8)[:,:,:3])
    
    if notebook:
        plt.figure()
        plt.imshow(img)
        plt.axis('off')
        plt.figure()
        plt.imshow(pil_out)
        plt.axis('off')
    
    else:
        fig.add_subplot(4,4,2*idx+1)
        plt.imshow(img)
        plt.axis('off')
        fig.add_subplot(4,4,2*idx+2)
        plt.imshow(pil_out)
        plt.axis('off')