import numpy as np
import struct
#import os

def SaveRangeImageOld(filename, ranges, reflectances):
    assert ranges.shape == reflectances.shape
    with open(filename, 'wb') as rangefile:
        num_rows, num_cols = ranges.shape
        num_rows = struct.pack('<i', num_rows)
        rangefile.write(num_rows)
        num_cols = struct.pack('<i', num_cols)
        rangefile.write(num_cols)

        ranges = ranges.flatten().astype(np.float32)
        reflectances = reflectances.flatten().astype(np.float32)
        inds = np.nonzero(ranges)[0]
        num_data = struct.pack('<i', len(inds))
        rangefile.write(num_data)
        print(inds)
        for i in inds:
            rangefile.write(struct.pack('<i', i))
            rangefile.write(struct.pack('<f', ranges[i]))
            rangefile.write(struct.pack('<f', reflectances[i]))

def ReadRangeImageOld(filename):
    with open(filename, 'rb') as rangefile:
        data = rangefile.read()
        num_rows = struct.unpack_from('<i', data, offset=0)[0]
        num_cols = struct.unpack_from('<i', data, offset=4)[0]
        num_data = struct.unpack_from('<i', data, offset=8)[0]
        print(num_rows, num_cols, num_data)
        N = num_rows*num_cols
        ranges = np.zeros(N, dtype=np.float32)
        reflectances = np.zeros(N, dtype=np.float32)
        
        for i in range(12, num_data*12+12, 12):
            print(i)
            loc = struct.unpack_from('<i', data, offset=i)[0]
            ranges[loc] = struct.unpack_from('<f', data, offset=i+4)[0]
            reflectances[loc] = struct.unpack_from('<f', data, offset=i+8)[0]

        ranges = np.reshape(ranges, (num_rows, num_cols))
        reflectances = np.reshape(reflectances, (num_rows, num_cols))

    return ranges, reflectances


def SaveRangeImage(filename, num_rows, num_cols, pix_x, pix_y, ranges, reflectances):
    #assert ranges.shape == reflectances.shape
    with open(filename, 'wb') as rangefile:
        N = len(ranges)
        num_data = struct.pack('<i', N)
        rangefile.write(num_data)
        num_rows = struct.pack('<i', num_rows)
        rangefile.write(num_rows)
        num_cols = struct.pack('<i', num_cols)
        rangefile.write(num_cols)

        #ranges = ranges.astype(np.float32)
        #reflectances = reflectances.astype(np.float32)
        for i in range(N):
            rangefile.write(struct.pack('<H', pix_x[i].astype(np.ushort)))
            rangefile.write(struct.pack('<H', pix_y[i].astype(np.ushort)))
            rangefile.write(struct.pack('<f', ranges[i].astype(np.float32)))
            rangefile.write(struct.pack('<f', reflectances[i].astype(np.float32)))

def ReadRangeImage(filename, ret_image=False):
    with open(filename, 'rb') as rangefile:
        data = rangefile.read()
        N = struct.unpack_from('<i', data, offset=0)[0]
        num_rows = struct.unpack_from('<i', data, offset=4)[0]
        num_cols = struct.unpack_from('<i', data, offset=8)[0]
        #print(num_rows, num_cols, N)
        pix_x = np.zeros(N, dtype=np.ushort)
        pix_y = np.zeros(N, dtype=np.ushort)
        ranges = np.zeros(N, dtype=np.float32)
        reflectances = np.zeros(N, dtype=np.float32)
        
        j = 0
        for i in range(12, N*12+12, 12):
            #print i
            pix_x[j] = struct.unpack_from('<H', data, offset=i)[0]
            pix_y[j] = struct.unpack_from('<H', data, offset=i+2)[0]
            ranges[j] = struct.unpack_from('<f', data, offset=i+4)[0]
            reflectances[j] = struct.unpack_from('<f', data, offset=i+8)[0]
            j += 1

    if ret_image:
        ret = np.zeros((num_rows, num_cols, 2), dtype=np.float32)
        for i in range(N):
            ret[pix_y[i], pix_x[i], 0] = ranges[i].astype(np.float32)
            ret[pix_y[i], pix_x[i], 1] = reflectances[i].astype(np.float32)
        return ret

    return num_rows, num_cols, pix_x, pix_y, ranges, reflectances

