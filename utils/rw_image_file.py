import numpy as np
import struct
#import os



def SaveRWImage(filename, num_rows, num_cols, pixel_x, pixel_y, data_dict):
    assert len(pixel_x) == len(pixel_y)
    with open(filename, 'wb') as rwfile:
        N = len(pixel_x)
        num_data = struct.pack('<i', N)
        rwfile.write(num_data)
        num_rows = struct.pack('<i', num_rows)
        rwfile.write(num_rows)
        num_cols = struct.pack('<i', num_cols)
        rwfile.write(num_cols)

        # tsekkaa onko mikakin datalaji data_dictissa, tallenna booleanit ja tallenna sitten datat jarjestyksessa
        '''
        SurfaceT_b = False
        State_b = False
        Grip_b = False
        Water_b = False
        Ice_b = False
        Snow_b = False
        EN15518State_b = False
        AirT_b = False
        RH_b = False
        DewPoint_b = False
        FrostPoint_b = False
        DataWarning_b = False
        DataError_b = False
        UnitStatus_b = False
        ErrorBits_b = False
        '''

        # Write booleans that tell which datas are in the file
        if 'SurfaceT' in data_dict.keys():
            #SurfaceT_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'State' in data_dict.keys():
            #State_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'Grip' in data_dict.keys():
            #Grip_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'Water' in data_dict.keys():
            #Water_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'Ice' in data_dict.keys():
            #Ice_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'Snow' in data_dict.keys():
            #Snow_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'EN15518State' in data_dict.keys():
            #EN15518State_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'AirT' in data_dict.keys():
            #AirT_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'RH' in data_dict.keys():
            #RH_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'DewPoint' in data_dict.keys():
            #DewPoint_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'FrostPoint' in data_dict.keys():
            #FrostPoint_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'DataWarning' in data_dict.keys():
            #DataWarning_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'DataError' in data_dict.keys():
            #DataError_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'UnitStatus' in data_dict.keys():
            #UnitStatus_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))
        if 'ErrorBits' in data_dict.keys():
            #ErrorBits_b = True
            rwfile.write(struct.pack('<?', True))
        else:
            rwfile.write(struct.pack('<?', False))

        # WRITE ACTUAL DATA #

        for pix_x in pixel_x:
            rwfile.write(struct.pack('<H', pix_x))
        for pix_y in pixel_y:
            rwfile.write(struct.pack('<H', pix_y))

        if 'SurfaceT' in data_dict.keys():
            for d in data_dict['SurfaceT'].astype(np.float32):
                rwfile.write(struct.pack('<f', d))
        if 'State' in data_dict.keys():
            for d in data_dict['State'].astype(np.ubyte):
                rwfile.write(struct.pack('<B', d))
        if 'Grip' in data_dict.keys():
            for d in data_dict['Grip'].astype(np.float32):
                rwfile.write(struct.pack('<f', d))
        if 'Water' in data_dict.keys():
            for d in data_dict['Water'].astype(np.float32):
                rwfile.write(struct.pack('<f', d))
        if 'Ice' in data_dict.keys():
            for d in data_dict['Ice'].astype(np.float32):
                rwfile.write(struct.pack('<f', d))
        if 'Snow' in data_dict.keys():
            for d in data_dict['Snow'].astype(np.float32):
                rwfile.write(struct.pack('<f', d))
        if 'EN15518State' in data_dict.keys():
            for d in data_dict['EN15518State'].astype(np.ubyte):
                rwfile.write(struct.pack('<B', d))
        if 'AirT' in data_dict.keys():
            for d in data_dict['AirT'].astype(np.float32):
                rwfile.write(struct.pack('<f', d))
        if 'RH' in data_dict.keys():
            for d in data_dict['RH'].astype(np.float32):
                rwfile.write(struct.pack('<f', d))
        if 'DewPoint' in data_dict.keys():
            for d in data_dict['DewPoint'].astype(np.float32):
                rwfile.write(struct.pack('<f', d))
        if 'FrostPoint' in data_dict.keys():
            for d in data_dict['FrostPoint'].astype(np.float32):
                rwfile.write(struct.pack('<f', d))
        if 'DataWarning' in data_dict.keys():
            for d in data_dict['DataWarning'].astype(np.ushort):
                rwfile.write(struct.pack('<H', d))
        if 'DataError' in data_dict.keys():
            for d in data_dict['DataError'].astype(np.ushort):
                rwfile.write(struct.pack('<H', d))
        if 'UnitStatus' in data_dict.keys():
            for d in data_dict['UnitStatus'].astype(np.uintc):
                rwfile.write(struct.pack('<I', d))
        if 'ErrorBits' in data_dict.keys():
            for d in data_dict['ErrorBits'].astype(np.uintc):
                rwfile.write(struct.pack('<I', d))

def ReadRWImage(filename, out_datas=None, ret_image=False):
    '''
    Reads data from a road weather file.

    NOTE: ret_image and out_datas have always the following (channel) order, even though some datas would be missing:
    SurfaceT, State, Grip, Water, Ice, Snow, EN15518State, AirT, RH, DewPoint, FrostPoint, DataWarning, DataError, UnitStatus, ErrorBits
    '''
    with open(filename, 'rb') as rwfile:
        data = rwfile.read()
        N = struct.unpack_from('<i', data, offset=0)[0]
        num_rows = struct.unpack_from('<i', data, offset=4)[0]
        num_cols = struct.unpack_from('<i', data, offset=8)[0]

        #print(N, num_rows, num_cols)

        SurfaceT_b = struct.unpack_from('<?', data, offset=12)[0]
        State_b = struct.unpack_from('<?', data, offset=13)[0]
        Grip_b = struct.unpack_from('<?', data, offset=14)[0]
        Water_b = struct.unpack_from('<?', data, offset=15)[0]
        Ice_b = struct.unpack_from('<?', data, offset=16)[0]
        Snow_b = struct.unpack_from('<?', data, offset=17)[0]
        EN15518State_b = struct.unpack_from('<?', data, offset=18)[0]
        AirT_b = struct.unpack_from('<?', data, offset=19)[0]
        RH_b = struct.unpack_from('<?', data, offset=20)[0]
        DewPoint_b = struct.unpack_from('<?', data, offset=21)[0]
        FrostPoint_b = struct.unpack_from('<?', data, offset=22)[0]
        DataWarning_b = struct.unpack_from('<?', data, offset=23)[0]
        DataError_b = struct.unpack_from('<?', data, offset=24)[0]
        UnitStatus_b = struct.unpack_from('<?', data, offset=25)[0]
        ErrorBits_b = struct.unpack_from('<?', data, offset=26)[0]

        out_list = ()

        start = 27
        size = 2
        pixel_x = np.zeros(N, dtype=np.ushort)
        j = 0
        for i in range(start, N*size+start, size):
            d = struct.unpack_from('<H', data, offset=i)[0]
            pixel_x[j] = d
            j += 1
        start = start + N*size
        size = 2
        pixel_y = np.zeros(N, dtype=np.ushort)
        j = 0
        for i in range(start, N*size+start, size):
            d = struct.unpack_from('<H', data, offset=i)[0]
            pixel_y[j] = d
            j += 1
        start = start + N*size
        if SurfaceT_b:
            size = 4
            SurfaceT = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<f', data, offset=i)[0]
                SurfaceT[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'SurfaceT' in out_datas: out_list += (SurfaceT,)
        if State_b:
            size = 1
            State = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<B', data, offset=i)[0]
                State[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'State' in out_datas: out_list += (State,)
        if Grip_b:
            size = 4
            Grip = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<f', data, offset=i)[0]
                Grip[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'Grip' in out_datas: out_list += (Grip,)
        if Water_b:
            size = 4
            Water = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<f', data, offset=i)[0]
                Water[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'Water' in out_datas: out_list += (Water,)
        if Ice_b:
            size = 4
            Ice = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<f', data, offset=i)[0]
                Ice[j] = d
                j += 1
            start = start + N*size       
            if not out_datas or 'Ice' in out_datas: out_list += (Ice,)
        if Snow_b:
            size = 4
            Snow = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<f', data, offset=i)[0]
                Snow[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'Snow' in out_datas: out_list += (Snow,)
        if EN15518State_b:
            size = 1
            EN15518State = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<B', data, offset=i)[0]
                EN15518State[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'EN15518State' in out_datas: out_list += (EN15518State,)
        if AirT_b:
            size = 4
            AirT = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<f', data, offset=i)[0]
                AirT[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'AirT' in out_datas: out_list += (AirT,)
        if RH_b:
            size = 4
            RH = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<f', data, offset=i)[0]
                RH[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'RH' in out_datas: out_list += (RH,)
        if DewPoint_b:
            size = 4
            DewPoint = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<f', data, offset=i)[0]
                DewPoint[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'DewPoint' in out_datas: out_list += (DewPoint,)
        if FrostPoint_b:
            size = 4
            FrostPoint = np.zeros(N, dtype=np.float32)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<f', data, offset=i)[0]
                FrostPoint[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'FrostPoint' in out_datas: out_list += (FrostPoint,)
        if DataWarning_b:
            size = 2
            DataWarning = np.zeros(N, dtype=np.ushort)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<H', data, offset=i)[0]
                DataWarning[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'DataWarning' in out_datas: out_list += (DataWarning,)
        if DataError_b:
            size = 2
            DataError = np.zeros(N, dtype=np.ushort)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<H', data, offset=i)[0]
                DataError[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'DataError' in out_datas: out_list += (DataError,)
        if UnitStatus_b:
            size = 4
            UnitStatus = np.zeros(N, dtype=np.uintc)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<I', data, offset=i)[0]
                UnitStatus[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'UnitStatus' in out_datas: out_list += (UnitStatus,)
        if ErrorBits_b:
            size = 4
            ErrorBits = np.zeros(N, dtype=np.uintc)
            j = 0
            for i in range(start, N*size+start, size):
                d = struct.unpack_from('<I', data, offset=i)[0]
                ErrorBits[j] = d
                j += 1
            start = start + N*size
            if not out_datas or 'ErrorBits' in out_datas: out_list += (ErrorBits,)

    if ret_image:
        ret = np.zeros((num_rows, num_cols, len(out_list)), dtype=np.float32)
        for i in range(N):
            for j in range(len(out_list)):
                ret[pixel_y[i], pixel_x[i], j] = out_list[j][i].astype(np.float32)
        return ret

    return pixel_x, pixel_y, out_list

